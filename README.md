# Drupal 8.5.5 Project Base

#### install
1. composer install
2. drupal list
3. drupal site:install
4. 


#### Manual enable module
1. Devel generate
2. Devel Kint
3. RESTful Web Services
4. Statistics
5. Webform
5. Admin-toolbar


#### Debug Mode
1. chmod -R u+w sites
2. chmod -Rf 775 sites/default
3. Sites/default/settings.php 785 line un comment
4. settings.local.php change to koro.settings.php
4. cp sites/example.settings.local.php sites/settings.local.php
5. $config['system.performance']['js']['preprocess'] = FALSE;
6. $config['system.performance']['css']['preprocess'] = FALSE;
7. admin/config/development/devel Rebuild the theme registry on every page load
8. enable twig debug /sites/settings.php find $settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml' change to $settings['container_yamls'][] = $app_root .  '/sites/koro.services.yml'
9. copy /sites/default/default.services.yml to /sites/koro.services.yml
10 drupal cr clear cache